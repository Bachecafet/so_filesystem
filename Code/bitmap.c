#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "bitmap.h"

int BitMap_getStatus(BitMap* bmap, int pos){
	
	if(pos >= bmap->num_bits) return -1;
	
	BitMapEntryKey map = BitMap_blockToIndex(pos);
	return bmap->entries[map.entry_num] >> map.bit_num & mask;			//ritorno lo stato

}

// converts a block index to an index in the array,
// and a char that indicates the offset of the bit inside the array
BitMapEntryKey BitMap_blockToIndex(int num){
	
	BitMapEntryKey bmap;
	int entry = num / bit_in_byte;					//numero di byte
	char offset = num - (entry * bit_in_byte);		//offset bit in byte
	
	bmap.entry_num = entry;
	bmap.bit_num = offset;
	
	return bmap;
}


// converts a bit to a linear index
int BitMap_indexToBlock(int entry, uint8_t bit_num){

	//error handler
	if(entry < 0 || bit_num < 0) return -1;
	
	
	int num = (entry * bit_in_byte) + bit_num;
	return num;
}


// returns the index of the first bit having status "status"
// in the bitmap bmap, and starts looking from position start
int BitMap_get(BitMap* bmap, int start, int status){				//io so che status = bmap->entries[entry_num]

	if(start > bmap->num_bits) return -1;							//error handler, controllo se si parte da un 
																	//punto valido
	int pos = start;																
	while(pos < bmap->num_bits){
		if(BitMap_getStatus(bmap, pos) == status)	return pos;
		pos++;
	}
	
	return -1;
}	

// sets the bit at index pos in bmap to status
int BitMap_set(BitMap* bmap, int pos, int status){

	if(pos > bmap->num_bits) return -1;
	
	BitMapEntryKey map = BitMap_blockToIndex(pos);
	unsigned char for_change = 1 << map.bit_num;
	unsigned char to_change = bmap->entries[map.entry_num];
	
	if(status == 1){
        bmap->entries[map.entry_num] = to_change | for_change;          //mask by OR to have 1
        return to_change | for_change;
    }
    else{
        bmap->entries[map.entry_num] = to_change & (~for_change);        //mask by AND to have 0 (with !mask)
        return to_change & (~for_change);
    }
}




























